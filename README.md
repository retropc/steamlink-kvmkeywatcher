# steamlink keyboard watcher utility

## functionality

- pressing numlock twice switches kvm port (with cheap TK-207K USB KVM)
- pressing printscreen twice reboots steamlink

## installing from binary

- copy downloaded .tar.gz file to FAT32 formatted USB stick into /steamlink/apps/ directory
- plug USB stick into steamlink, reboot
- should be "Install kvmkeywatcher" on menu, run it
- device should now reboot
- functionality should now work

## building yourself

- download steamlink SDK: https://github.com/valvesoftware/steamlink-sdk

```
cd /path/to/steamlink/sdk
source ./setenv.sh
cd /path/to/kvmkeywatcher
make
```

