CFLAGS+=-Wall -Wno-unused
.PHONY: clean all

all: kvmkeywatcher S99kvmkeywatcher-arm kvmkeywatcher.tar.gz

clean:
	rm -f kvmkeywatcher S99kvmkeywatcher-arm kvmkeywatcher.tar.gz

kvmkeywatcher: kvmkeywatcher.c usb.c
	gcc $(CFLAGS) -pedantic -DDEBUG $(LDFLAGS) -o $@ $^ -lusb

S99kvmkeywatcher-arm: kvmkeywatcher.c usb.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ -lusb -s

kvmkeywatcher.tar.gz: S99kvmkeywatcher-arm icon.png toc.txt install.sh
	tar --transform="s/^/kvmkeywatcher\//" --group=root --owner=root -zcf $@ $^
